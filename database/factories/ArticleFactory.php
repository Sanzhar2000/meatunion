<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name_ru' => $this->faker->sentence,
            'annotation_ru' => $this->faker->text,
            'description_ru' => $this->faker->paragraph(6),
            'avatar_ru' => $this->faker->imageUrl(640, 480, 'dogs'),
            'published_at' => $this->faker->dateTimeBetween(),
            'is_published' => true,
        ];
    }
}
