<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn(['name']);
            $table->string('fio')->after('id')->nullable();
            $table->string('iin')->after('fio')->nullable();
            $table->boolean('isMember')->after('iin')->nullable();

            $table->timestamp('deleted_at')->nullable()->after('remember_token');

            $table->unsignedBigInteger('created_by')->index()->nullable()->after('remember_token');
            $table->unsignedBigInteger('modified_by')->index()->nullable()->after('remember_token');
            $table->unsignedBigInteger('deleted_by')->index()->nullable()->after('remember_token');

            $table->foreign('created_by')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('modified_by')->references('id')->on('users')->onDelete('SET NULL');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
