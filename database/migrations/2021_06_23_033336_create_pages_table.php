<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_kk')->nullable();
            $table->string('title_en')->nullable();
            $table->longText('description_ru')->nullable();
            $table->longText('description_kk')->nullable();
            $table->longText('description_en')->nullable();
            $table->string('img_ru')->nullable();
            $table->string('img_kk')->nullable();
            $table->string('img_en')->nullable();

            $table->longText('appendix_ru')->nullable();
            $table->longText('appendix_kk')->nullable();
            $table->longText('appendix_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
