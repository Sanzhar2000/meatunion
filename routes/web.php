<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin'], function () {

    Route::get('/auth', function (){
        return view('flowdock.authorization');
    });

    Route::get('/fund-detail-edit', function (){
        return view('flowdock.fund-detail-edit');
    });

    Route::get('/fund-detail-view', function (){
        return view('flowdock.fund-detail-view');
    });

    Route::get('/index', function (){
        return view('flowdock.index');
    });

    Route::get('/search-page', function (){
        return view('flowdock.search-page');
    });
    Route::get('/test', function (){
        return view('flowdock.test');
    });
    Route::get('/user', function (){
        return view('flowdock.user');
    });

    Route::get('user777', function () {

        return view('admin.pages.users.index');
    });

});

//    Route::group(['prefix' => 'admin'], function () {
//
//    Route::prefix('profile')->name('profile.')->group(function () {
//        Route::get('/', [ProfileController::class, 'index'])->name('index');
//        Route::post('/', [ProfileController::class, 'update'])->name('update');
//    });
//
//        Route::prefix('articles')->name('articles.')->group(function () {
//
//            Route::get('/create', [ArticleController::class, 'create'])->name('create');
//            Route::post('/create', [ArticleController::class, 'store'])->name('store');
//
//            Route::get('/', [ArticleController::class, 'index'])->name('index');
//            Route::get('/{item}', [ArticleController::class, 'edit'])->name('edit');
//
//            Route::post('/{item}', [ArticleController::class, 'update'])->name('update');
//
//            Route::delete('/{item}', [ArticleController::class, 'delete'])->name('delete');
//        });
//
//    });

