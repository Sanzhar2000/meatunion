<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


///**
// * App\Models\Article
// *
// * @property int $id
// * @property string|null $name_ru
// * @property string|null $name_kk
// * @property string|null $name_en
// * @property string|null $annotation_ru
// * @property string|null $annotation_kk
// * @property string|null $annotation_en
// * @property string|null $description_ru
// * @property string|null $description_kk
// * @property string|null $description_en
// * @property string|null $avatar_ru
// * @property string|null $avatar_kk
// * @property string|null $avatar_en
// * @property bool $is_published
// * @property \Illuminate\Support\Carbon|null $published_at
// * @property int|null $created_by
// * @property int|null $modified_by
// * @property int|null $deleted_by
// * @property \Illuminate\Support\Carbon|null $created_at
// * @property \Illuminate\Support\Carbon|null $updated_at
// * @property string|null $deleted_at
// * @property-read \App\Models\User|null $creator
// * @property-read \App\Models\User|null $modifier
// * @property-read \App\Models\User|null $remover
// * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
// * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
// * @method static \Illuminate\Database\Eloquent\Builder|Article query()
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAnnotationEn($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAnnotationKk($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAnnotationRu($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAvatarEn($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAvatarKk($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereAvatarRu($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedBy($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereDeletedAt($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereDeletedBy($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereDescriptionEn($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereDescriptionKk($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereDescriptionRu($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereIsPublished($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereModifiedBy($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereNameEn($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereNameKk($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereNameRu($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article wherePublishedAt($value)
// * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
// * @mixin \Eloquent
// */
class Article extends Model
{
    use HasFactory;

//    protected $table = 'articles';
//
//    public $timestamps = true;
//
//    protected $fillable = [
//        'name_ru',
//        'name_kk',
//        'name_en',
//        'annotation_ru',
//        'annotation_kk',
//        'annotation_en',
//        'description_ru',
//        'description_kk',
//        'description_en',
//        'avatar_ru',
//        'avatar_kk',
//        'avatar_en',
//        'published_at',
//        'is_published',
//    ];
//
//    protected $casts = [
//        'published_at' => 'date',
//        'is_published' => 'boolean',
//    ];
//
//    public function creator()
//    {
//        return $this->belongsTo(User::class, 'created_by')->where('users.deleted_at', '=', null);
//    }
//
//    public function modifier()
//    {
//        return $this->belongsTo(User::class, 'modified_by')->where('users.deleted_at', '=', null);
//    }
//
//    public function remover()
//    {
//        return $this->belongsTo(User::class, 'deleted_by')->where('users.deleted_at', '=', null);
//    }
//
//    public function getAttribute($key)
//    {
//        $array = ['name', 'annotation', 'description', 'avatar'];
//        if (in_array($key, $array)) {
//            if (!empty(parent::getAttribute($key . '_' . App::getLocale()))) {
//                $key .= '_' . App::getLocale();
//            } else {
//                $key .= '_ru';
//            }
//        }
//
//        return parent::getAttribute($key);
//    }
}
