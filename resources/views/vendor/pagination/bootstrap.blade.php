@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><span class="page-link">&laquo;</span></li>
        @else
            @php
                $url = $paginator->previousPageUrl();
                $url = parse_url($url);
                $query = isset($url['query']) ? $url['query'] : '';
                $query = explode('&',$query);
                $page = null;
                foreach ($query as $q){
                    $temp = explode('=',$q);
                    $page = $temp[0] === 'page' ? $temp[1] : $page;
                }
            @endphp
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                                     data-page-num="{{ $page }}">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page === $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}"
                                                 data-page-num="{{ $page }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            @php
                $url = $paginator->nextPageUrl();
                $url = parse_url($url);
                $query = isset($url['query']) ? $url['query'] : '';
                $query = explode('&',$query);
                $page = null;
                foreach ($query as $q){
                    $temp = explode('=',$q);
                    $page = $temp[0] === 'page' ? $temp[1] : $page;
                }
            @endphp
            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"
                                     data-page-num="{{ $page }}">&raquo;</a></li>
        @else
            <li class="page-item disabled"><span class="page-link">&raquo;</span></li>
        @endif
    </ul>
@endif
