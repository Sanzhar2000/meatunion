@extends('layouts.admin')

@section('title', _('Laravel'))

@section('content')
    <div class="container container-fluid">
        <div class="title-block">
            <div class="row row--multiline align-items-center">
                <div class="col-md-4">
                    <h1 class="title-primary" style="margin-bottom: 0">Архив</h1>
                </div>
                <div class="col-md-8 text-right-md text-right-lg">
                    <div class="flex-form">
                        <div>
                            <a href="#" title="Расширенный поиск" class="btn"><i class="icon-search"></i> <span>Расширенный поиск</span></a>
                        </div>
                        <div>
                            <form class="input-button">
                                <input type="text" name="search" placeholder="Наименование фонда/Номер фонда" class="input-regular input-regular--solid" style="width: 282px;">
                                <button class="btn btn--green">Найти</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <h2 class="title-secondary">Список записей</h2>
            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 12%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Наименование фонда</th>
                    <th>Номер фонда</th>
                    <th>Организация</th>
                    <th>Дата создания</th>
                    <th>Дата изменения</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>67</td>
                    <td>Langosh PLC	</td>
                    <td>958000</td>
                    <td>Государственное учреждение "Управление цифровизации, оказания государственных услуг и архивов Туркестанской области"</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19 Администратор Panama DC</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>68</td>
                    <td>Moore-Barton</td>
                    <td>443812</td>
                    <td>Туркестанский областной государственный архив	</td>
                    <td>2020-01-15 12:40:16	</td>
                    <td>2020-01-22 13:19:19</td>
                    <td>
                        <div class="action-buttons">
                            <a href="#" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="#" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="#" title="Удалить" class="icon-btn icon-btn--pink icon-delete"></a>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="text-right">
                <ul class="pagination">
                    <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li class="dots disabled"><span>...</span></li>
                    <li><a href="#">498</a></li>
                    <li><a href="#">499</a></li>
                    <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
                </ul>
            </div>
        </div>

        <div class="block collapsed">
            <div class="block__header">
                <h2 class="title-secondary">Отчет</h2>
                <i class="icon-chevron-up btn-collapse"></i>
            </div>

            <div class="block__body" style="display:none;">
                <table class="table report">
                    <thead class="gray">
                    <tr>
                        <th colspan="2"><i>Фондов зарегистрировано:</i> <strong>50</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><strong>на организацию "Шымкентский городской государственный архив"</strong></td>
                        <td>
                            <div class="line-chart" data-value="3"><span></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>на организацию "Государственное учреждение "Управление цифровизации, оказания государственных услуг и архивов Туркестанской области""</strong></td>
                        <td>
                            <div class="line-chart" data-value="2"><span></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>на организацию "Сарыагаш"</strong></td>
                        <td>
                            <div class="line-chart" data-value="3"><span></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>на организацию "Туркестанский областной государственный архив"</strong></td>
                        <td>
                            <div class="line-chart" data-value="1"><span></span></div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>на организацию "Жетысайский региональный государственный архив"</strong></td>
                        <td>
                            <div class="line-chart" data-value="4"><span></span></div>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot class="gray">
                    <tr>
                        <td colspan="2">
                            <i>Фондов отредактировано:</i> <strong>1</strong><br/>
                            <i>Фондов удалено:</i> <strong>0</strong><br/>
                            <i>Особо ценных фондов:</i> <strong>0</strong>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!--Only this page's scripts-->

    <!---->

    <div id="message" class="modal" style="display: none;">
        <h4 class="title-secondary">Удаление</h4>
        <div class="plain-text">
            При удалении все данные будут удалены
        </div>
        <hr>
        <div class="buttons justify-end">
            <div><button class="btn btn--red">Удалить</button></div>
            <div><button class="btn" data-fancybox-close>Отмена</button></div>
        </div>
    </div>

    <!--
    <script>
        $.fancybox.open({
          src: "#message",
          touch: false
        })
    </script>-->
@endsection
