<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layout.partials.components.head')
</head>
<body>

<input id="lang" type="hidden" value="ru">
<div class="main-wrapper">
    @include('admin.layout.partials.components.sidebar')

    <div class="right-wrapper">
        @include('admin.layout.partials.components.header')

        <main class="main">


            @yield('content')

        </main>

        @include('admin.layout.partials.components.footer')
    </div>
</div>

@include('admin.layout.partials.components.scripts')

@yield('scripts')


</body>
</html>
